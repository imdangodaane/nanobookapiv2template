START POSTGRES DOCKER
```
docker run -p 5432:5432 --name nanobook-postgres -e POSTGRES_USER=nanobookdev -e POSTGRES_PASSWORD=nanobookdev -e POSTGRES_DB=nanobookdev -d postgres:13-alpine
```

```
CREATE ROLE root LOGIN SUPERUSER;
CREATE ROLE postgres LOGIN SUPERUSER;
CREATE ROLE nanobook LOGIN SUPERUSER;
```
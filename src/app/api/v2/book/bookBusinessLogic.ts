import { injectable } from 'inversify';
import { IBookBusinessLogic } from './bookInterface';

@injectable()
export class BookBusinessLogic implements IBookBusinessLogic {
  constructor() {
  }

  addBook(): void {
    throw new Error('Method not implemented.');
  }
  getBookDetails(): void {
    throw new Error('Method not implemented.');
  }

}

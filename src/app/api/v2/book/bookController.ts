import { RequestHandler } from 'express';
import { injectable } from 'inversify';
import { IBookController } from './bookInterface';
import { getRepository } from 'typeorm';
import { Books } from '#entities/Books';

@injectable()
export class BookController implements IBookController {
  constructor() {}

  addBook(): RequestHandler {
    return async (req: any, res: any) => {
      res.status(200).json('Add book API');
    };
  }

  getBookDetails(): RequestHandler {
    return async (req: any, res: any) => {
      try {
        const allBook = await getRepository(Books).find();
        res.status(200).json(
          allBook,
        );
      } catch (err) {
        res.status(500).json('Internal server error');
      }
    };
  }
}

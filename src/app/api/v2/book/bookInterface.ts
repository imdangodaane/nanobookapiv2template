import { RequestHandler } from 'express';

export interface IBookController {
  addBook(): RequestHandler;
  getBookDetails(): RequestHandler;
}

export interface IBookBusinessLogic {
  addBook(): void;
  getBookDetails(): void;
}

export interface IBookDbAccess {

}

import express from 'express';
import cors from 'cors';
import compression from 'compression';

import { Container } from 'inversify';
import { routes } from './routes';

export const expressLoader = (app: express.Express, container: Container) => {
  // Setting up middleware
  app.use(cors());
  app.use(compression());
  app.use(express.raw());
  app.use(express.json());

  // Main routes
  app.use('/', routes(container));

  return app;
};

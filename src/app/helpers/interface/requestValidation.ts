import { RequestHandler } from 'express';

export interface IRequestValidation {
  deviceUIDValidation(): RequestHandler;
}

import { injectable } from 'inversify';
import { IRequestValidation } from './interface/requestValidation';

@injectable()
export class RequestValidation implements IRequestValidation {
  public supportLanguages: String[] = ['vi_VN', 'en_US'];

  constructor() {}

  deviceUIDValidation() {
    return (req: any, res: any, next: any) => {
      if (!req.headers['language'] || !this.supportLanguages.includes(req.headers.language)) {
        req.headers['language'] = this.supportLanguages[0];
      }
      if (!req.headers['device_uid']) {
        return res.status(400).json({
          isSuccess: false,
          message: 'Device UID required',
        });
      }
      next();
    };
  }
}

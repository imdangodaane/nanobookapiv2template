import 'reflect-metadata';
import * as dotenv from 'dotenv';
import express from 'express';
import { Server } from 'http';
import { expressLoader } from './express';
import { iocLoader } from './ioc';
import { typeormLoader } from './typeorm';
import { accessEnv } from '#helpers/accessEnvironment';

export class App {
  app: express.Express;

  constructor() {
    this.preloadConfiguration();
    this.app = express();
  }

  async start(): Promise<Server | undefined> {
    // Load container (dependency injection)
    const container = iocLoader();

    // Create connection to Postgres DB
    const DB_PORT = accessEnv('DB_PORT');
    if (!DB_PORT) {
      console.error(`[${new Date().toLocaleString()}]\tUnable to resolve Database port}`);
      return;
    }
    const postgresConnection = await typeormLoader(Number(DB_PORT));
    console.log(`[${new Date().toLocaleString()}]\tPostgres server connected: ${DB_PORT}`);

    // Load express config and routes
    expressLoader(this.app, container);

    // Server listening
    const EXPRESS_PORT = accessEnv('APP_PORT');
    if (!EXPRESS_PORT) {
      console.error(`[${new Date().toLocaleString()}]\tUnable to resolve express server port}`);
      return;
    }
    const expressServer: Server = this.app.listen(EXPRESS_PORT, () => {
      console.log(`[${new Date().toLocaleString()}]\tExpress server started and listen on PORT: ${EXPRESS_PORT}`);
    });

    return expressServer;
  }

  stop(server: Server): Server {
    return server.close();
  }

  preloadConfiguration(): void {
    // Load environment variables from .env file
    dotenv.config({ path: 'src/config/.env' });
  }
}

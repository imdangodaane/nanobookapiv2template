import { Container } from 'inversify';
import { BookController } from './api/v2/book/bookController';
import { IBookController } from './api/v2/book/bookInterface';
import { RequestValidation } from '#helpers/requestValidation';
import { IRequestValidation } from '#helpers/interface/requestValidation';

export const iocLoader = () => {
  const container: Container = new Container();

  container.bind<IBookController>('IBookController').to(BookController);
  container.bind<IRequestValidation>('IRequestValidation').to(RequestValidation);

  return container;
};

import express from 'express';

import { apiV2Routes } from 'app/routes/routesv2/routesv2';
import { Container } from 'inversify';

export const routes = (container: Container) => {
  const router = express.Router();

  router.use('/api/v2', apiV2Routes(container));

  return router;
};

import express from 'express';

import { Container } from 'inversify';
import { IBookController } from '../../api/v2/book/bookInterface';


export const bookRoutes = (container: Container) => {
  const router = express.Router();
  const bookController = container.get<IBookController>('IBookController');

  // * BOOKS
  router
    .get('/:book_id', bookController.getBookDetails());
  router
    .post('/', bookController.addBook());

  return router;
};

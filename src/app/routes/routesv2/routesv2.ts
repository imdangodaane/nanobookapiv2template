import express from 'express';

import { Container } from 'inversify';
import { bookRoutes } from 'app/routes/routesv2/bookroutesv2';
import { IRequestValidation } from '#helpers/interface/requestValidation';

export const apiV2Routes = (container: Container) => {
  const router = express.Router();
  const reqValidation = container.get<IRequestValidation>('IRequestValidation');

  router.use('/book', reqValidation.deviceUIDValidation(), bookRoutes(container));

  // router.use('/book', bookRoutes(container));

  return router;
};

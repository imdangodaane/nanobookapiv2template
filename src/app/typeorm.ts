import { createConnection } from 'typeorm';
import { accessEnv } from './helpers/accessEnvironment';

export const typeormLoader = (DB_PORT: number) => {
  const connectionString = createConnection({
    type: 'postgres',
    host: accessEnv('DB_HOST'),
    port: DB_PORT,
    username: accessEnv('DB_USERNAME'),
    password: accessEnv('DB_PASSWORD'),
    database: accessEnv('DB_NAME'),
    synchronize: false,
    logging: false,
    entities: [
      'src/entities/**/*.ts',
    ],
    migrations: [
      'src/migration/**/*.ts',
    ],
    subscribers: [
      'src/subscriber/**/*.ts',
    ],
  });

  return connectionString;
};

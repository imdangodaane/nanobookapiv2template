import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('agreements_pkey', ['id'], { unique: true })
@Entity('agreements', { schema: 'public' })
export class Agreements {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'code', length: 100 })
  code!: string;

  @Column('text', { name: 'content', nullable: true })
  content!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;
}

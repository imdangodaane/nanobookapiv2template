import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('book', { schema: 'public' })
export class Book {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'title' })
  title!: string;

  @Column('character varying', { name: 'image', nullable: true })
  image!: string | null;

  @Column('text', { name: 'description' })
  description!: string;

  @Column('character varying', { name: 'author', nullable: true })
  author!: string | null;

  @Column('date', { name: 'publish_date' })
  publishDate!: string;

  @Column('character varying', { name: 'should_read', nullable: true })
  shouldRead!: string | null;

  @Column('character varying', { name: 'audio_file', nullable: true })
  audioFile!: string | null;

  @Column('boolean', { name: 'is_free', default: () => 'false' })
  isFree!: boolean;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => "'2020-10-05 00:55:26.165775+00'",
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => "'2020-10-05 00:55:26.165775+00'",
  })
  updatedAt!: Date;

  @Column('boolean', { name: 'is_on_home_banner', default: () => 'false' })
  isOnHomeBanner!: boolean;

  @Column('character varying', { name: 'banner_image' })
  bannerImage!: string;

  @Column('boolean', { name: 'is_free_book_of_day', default: () => 'false' })
  isFreeBookOfDay!: boolean;

  @Column('character varying', { name: 'short_description' })
  shortDescription!: string;

  @Column('integer', { name: 'duration' })
  duration!: number;

  @Column('text', { name: 'tiki_link' })
  tikiLink!: string;
}

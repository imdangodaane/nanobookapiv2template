import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { Categories } from './Categories';

@Index('book_categories_pkey', ['id'], { unique: true })
@Entity('book_categories', { schema: 'public' })
export class BookCategories {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @ManyToOne(() => Books, (books) => books.bookCategories)
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book!: Books;

  @ManyToOne(() => Categories, (categories) => categories.bookCategories)
  @JoinColumn([{ name: 'category_id', referencedColumnName: 'id' }])
  category!: Categories;
}

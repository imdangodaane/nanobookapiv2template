import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { BookHighlights } from './BookHighlights';

@Index('book_chapters_pkey', ['id'], { unique: true })
@Entity('book_chapters', { schema: 'public' })
export class BookChapters {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('integer', { name: 'order' })
  order!: number;

  @Column('character varying', { name: 'name', nullable: true, length: 255 })
  name!: string | null;

  @Column('text', { name: 'content', nullable: true })
  content!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @ManyToOne(() => Books, (books) => books.bookChapters, {
    onDelete: 'CASCADE',
  })
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book!: Books;

  @OneToMany(() => BookHighlights, (bookHighlights) => bookHighlights.chapter)
  bookHighlights!: BookHighlights[];
}

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { BookChapters } from './BookChapters';
import { Users } from './Users';

@Index('book_highlights_pkey', ['id'], { unique: true })
@Entity('book_highlights', { schema: 'public' })
export class BookHighlights {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('text', { name: 'note' })
  note!: string;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('integer', { name: 'start_index' })
  startIndex!: number;

  @Column('integer', { name: 'end_index' })
  endIndex!: number;

  @ManyToOne(() => Books, (books) => books.bookHighlights)
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book!: Books;

  @ManyToOne(() => BookChapters, (bookChapters) => bookChapters.bookHighlights)
  @JoinColumn([{ name: 'chapter_id', referencedColumnName: 'id' }])
  chapter!: BookChapters;

  @ManyToOne(() => Users, (users) => users.bookHighlights)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

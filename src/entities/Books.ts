import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BookCategories } from './BookCategories';
import { BookChapters } from './BookChapters';
import { BookHighlights } from './BookHighlights';
import { FavoriteBooks } from './FavoriteBooks';
import { Libraries } from './Libraries';
import { UserBookStatus } from './UserBookStatus';
import { UserFreeDailyBooks } from './UserFreeDailyBooks';

@Index('books_pkey', ['id'], { unique: true })
@Entity('books', { schema: 'public' })
export class Books {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'title', length: 255 })
  title!: string;

  @Column('character varying', { name: 'image', nullable: true, length: 255 })
  image!: string | null;

  @Column('text', { name: 'description', nullable: true })
  description!: string | null;

  @Column('character varying', { name: 'author', nullable: true, length: 255 })
  author!: string | null;

  @Column('date', { name: 'publish_date', nullable: true })
  publishDate!: string | null;

  @Column('character varying', {
    name: 'should_read',
    nullable: true,
    length: 255,
  })
  shouldRead!: string | null;

  @Column('character varying', {
    name: 'audio_file',
    nullable: true,
    length: 255,
  })
  audioFile!: string | null;

  @Column('boolean', { name: 'is_free', default: () => 'false' })
  isFree!: boolean;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('boolean', {
    name: 'is_on_home_banner',
    nullable: true,
    default: () => 'false',
  })
  isOnHomeBanner!: boolean | null;

  @Column('character varying', {
    name: 'banner_image',
    nullable: true,
    length: 255,
  })
  bannerImage!: string | null;

  @Column('boolean', {
    name: 'is_free_book_of_the_day',
    nullable: true,
    default: () => 'false',
  })
  isFreeBookOfTheDay!: boolean | null;

  @Column('character varying', {
    name: 'short_description',
    nullable: true,
    length: 255,
  })
  shortDescription!: string | null;

  @Column('integer', { name: 'duration', nullable: true })
  duration!: number | null;

  @Column('text', { name: 'tiki_link', nullable: true })
  tikiLink!: string | null;

  @OneToMany(() => BookCategories, (bookCategories) => bookCategories.book)
  bookCategories!: BookCategories[];

  @OneToMany(() => BookChapters, (bookChapters) => bookChapters.book)
  bookChapters!: BookChapters[];

  @OneToMany(() => BookHighlights, (bookHighlights) => bookHighlights.book)
  bookHighlights!: BookHighlights[];

  @OneToMany(() => FavoriteBooks, (favoriteBooks) => favoriteBooks.book)
  favoriteBooks!: FavoriteBooks[];

  @OneToMany(() => Libraries, (libraries) => libraries.book)
  libraries!: Libraries[];

  @OneToMany(() => UserBookStatus, (userBookStatus) => userBookStatus.book)
  userBookStatuses!: UserBookStatus[];

  @OneToMany(
    () => UserFreeDailyBooks,
    (userFreeDailyBooks) => userFreeDailyBooks.book,
  )
  userFreeDailyBooks!: UserFreeDailyBooks[];
}

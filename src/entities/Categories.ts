import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BookCategories } from './BookCategories';

@Index('categories_pkey', ['id'], { unique: true })
@Index('categories_name_unique', ['name'], { unique: true })
@Entity('categories', { schema: 'public' })
export class Categories {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'name', unique: true, length: 255 })
  name!: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('boolean', {
    name: 'is_on_home',
    nullable: true,
    default: () => 'false',
  })
  isOnHome!: boolean | null;

  @Column('boolean', {
    name: 'is_on_left_menu',
    nullable: true,
    default: () => 'false',
  })
  isOnLeftMenu!: boolean | null;

  @Column('character varying', {
    name: 'background_image',
    nullable: true,
    length: 255,
  })
  backgroundImage!: string | null;

  @OneToMany(() => BookCategories, (bookCategories) => bookCategories.category)
  bookCategories!: BookCategories[];
}

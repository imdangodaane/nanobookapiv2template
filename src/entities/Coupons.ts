import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Transactions } from './Transactions';
import { Users } from './Users';

@Index('coupons_code_unique', ['code'], { unique: true })
@Index('coupons_pkey', ['id'], { unique: true })
@Entity('coupons', { schema: 'public' })
export class Coupons {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'code', unique: true, length: 6 })
  code!: string;

  @Column('integer', { name: 'number_of_months', nullable: true })
  numberOfMonths!: number | null;

  @Column('timestamp with time zone', { name: 'expired_on', nullable: true })
  expiredOn!: Date | null;

  @Column('boolean', { name: 'is_used', default: () => 'false' })
  isUsed!: boolean;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('timestamp with time zone', { name: 'used_on', nullable: true })
  usedOn!: Date | null;

  @ManyToOne(() => Transactions, (transactions) => transactions.coupons)
  @JoinColumn([{ name: 'transaction_id', referencedColumnName: 'id' }])
  transaction!: Transactions;

  @ManyToOne(() => Users, (users) => users.coupons)
  @JoinColumn([{ name: 'used_by', referencedColumnName: 'id' }])
  usedBy!: Users;
}

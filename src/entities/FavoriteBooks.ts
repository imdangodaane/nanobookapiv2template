import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { Users } from './Users';

@Index('favorite_books_pkey', ['id'], { unique: true })
@Entity('favorite_books', { schema: 'public' })
export class FavoriteBooks {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @ManyToOne(() => Books, (books) => books.favoriteBooks)
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book!: Books;

  @ManyToOne(() => Users, (users) => users.favoriteBooks)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { Podcasts } from './Podcasts';
import { Users } from './Users';

@Index('libraries_pkey', ['id'], { unique: true })
@Entity('libraries', { schema: 'public' })
export class Libraries {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('text', { name: 'type' })
  type!: string;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('text', { name: 'status', nullable: true })
  status!: string | null;

  @Column('integer', { name: 'current_chapter', nullable: true })
  currentChapter!: number | null;

  @ManyToOne(() => Books, (books) => books.libraries)
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book!: Books;

  @ManyToOne(() => Podcasts, (podcasts) => podcasts.libraries)
  @JoinColumn([{ name: 'podcast_id', referencedColumnName: 'id' }])
  podcast!: Podcasts;

  @ManyToOne(() => Users, (users) => users.libraries)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Users } from './Users';

@Index('login_sessions_pkey', ['id'], { unique: true })
@Entity('login_sessions', { schema: 'public' })
export class LoginSessions {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('text', { name: 'access_token' })
  accessToken!: string;

  @Column('character varying', {
    name: 'device_uid',
    nullable: true,
    length: 255,
  })
  deviceUid!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('text', { name: 'fcm_device_token', nullable: true })
  fcmDeviceToken!: string | null;

  @ManyToOne(() => Users, (users) => users.loginSessions)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

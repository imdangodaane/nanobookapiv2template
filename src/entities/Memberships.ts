import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Users } from './Users';

@Index('memberships_code_unique', ['code'], { unique: true })
@Index('memberships_pkey', ['id'], { unique: true })
@Entity('memberships', { schema: 'public' })
export class Memberships {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'code', unique: true, length: 10 })
  code!: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('character varying', { name: 'title', nullable: true, length: 100 })
  title!: string | null;

  @OneToMany(() => Users, (users) => users.membership)
  users!: Users[];
}

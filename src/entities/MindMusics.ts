import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('mind_musics_pkey', ['id'], { unique: true })
@Entity('mind_musics', { schema: 'public' })
export class MindMusics {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'title', length: 100 })
  title!: string;

  @Column('character varying', { name: 'image', nullable: true, length: 255 })
  image!: string | null;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @Column('character varying', {
    name: 'filename',
    nullable: true,
    length: 255,
  })
  filename!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;
}

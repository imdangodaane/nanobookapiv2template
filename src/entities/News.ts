import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { NewsCategories } from './NewsCategories';

@Index('news_pkey', ['id'], { unique: true })
@Entity('news', { schema: 'public' })
export class News {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'title', nullable: true, length: 255 })
  title!: string | null;

  @Column('character varying', { name: 'image', nullable: true, length: 255 })
  image!: string | null;

  @Column('character varying', { name: 'author', nullable: true, length: 255 })
  author!: string | null;

  @Column('text', { name: 'content', nullable: true })
  content!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @ManyToOne(() => NewsCategories, (newsCategories) => newsCategories.news)
  @JoinColumn([{ name: 'category_id', referencedColumnName: 'id' }])
  category!: NewsCategories;
}

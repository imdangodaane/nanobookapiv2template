import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { News } from './News';

@Index('news_categories_pkey', ['id'], { unique: true })
@Index('news_categories_name_unique', ['name'], { unique: true })
@Entity('news_categories', { schema: 'public' })
export class NewsCategories {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', {
    name: 'name',
    nullable: true,
    unique: true,
    length: 100,
  })
  name!: string | null;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @Column('boolean', {
    name: 'is_on_left_menu',
    nullable: true,
    default: () => 'false',
  })
  isOnLeftMenu!: boolean | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @OneToMany(() => News, (news) => news.category)
  news!: News[];
}

import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Podcasts } from './Podcasts';

@Index('podcast_categories_pkey', ['id'], { unique: true })
@Index('podcast_categories_name_unique', ['name'], { unique: true })
@Entity('podcast_categories', { schema: 'public' })
export class PodcastCategories {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'name', unique: true, length: 100 })
  name!: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('character varying', {
    name: 'background_image',
    nullable: true,
    length: 255,
  })
  backgroundImage!: string | null;

  @OneToMany(() => Podcasts, (podcasts) => podcasts.category)
  podcasts!: Podcasts[];
}

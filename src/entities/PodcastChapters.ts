import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Podcasts } from './Podcasts';

@Index('podcast_chapters_pkey', ['id'], { unique: true })
@Entity('podcast_chapters', { schema: 'public' })
export class PodcastChapters {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'title', length: 255 })
  title!: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 500,
  })
  description!: string | null;

  @Column('character varying', {
    name: 'filename',
    nullable: true,
    length: 255,
  })
  filename!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('integer', { name: 'duration', nullable: true })
  duration!: number | null;

  @Column('integer', { name: 'order', nullable: true })
  order!: number | null;

  @ManyToOne(() => Podcasts, (podcasts) => podcasts.podcastChapters)
  @JoinColumn([{ name: 'podcast', referencedColumnName: 'id' }])
  podcast!: Podcasts;
}

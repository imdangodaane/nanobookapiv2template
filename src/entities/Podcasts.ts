import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Libraries } from './Libraries';
import { PodcastChapters } from './PodcastChapters';
import { PodcastCategories } from './PodcastCategories';

@Index('podcasts_pkey', ['id'], { unique: true })
@Entity('podcasts', { schema: 'public' })
export class Podcasts {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'title', length: 100 })
  title!: string;

  @Column('character varying', { name: 'image', nullable: true, length: 255 })
  image!: string | null;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 500,
  })
  description!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('boolean', {
    name: 'is_on_home_banner',
    nullable: true,
    default: () => 'false',
  })
  isOnHomeBanner!: boolean | null;

  @Column('character varying', {
    name: 'banner_image',
    nullable: true,
    length: 255,
  })
  bannerImage!: string | null;

  @OneToMany(() => Libraries, (libraries) => libraries.podcast)
  libraries!: Libraries[];

  @OneToMany(
    () => PodcastChapters,
    (podcastChapters) => podcastChapters.podcast,
  )
  podcastChapters!: PodcastChapters[];

  @ManyToOne(
    () => PodcastCategories,
    (podcastCategories) => podcastCategories.podcasts,
  )
  @JoinColumn([{ name: 'category', referencedColumnName: 'id' }])
  category!: PodcastCategories;
}

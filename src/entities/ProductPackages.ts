import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Transactions } from './Transactions';

@Index('product_packages_pkey', ['id'], { unique: true })
@Entity('product_packages', { schema: 'public' })
export class ProductPackages {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'code', length: 255 })
  code!: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 500,
  })
  description!: string | null;

  @Column('real', { name: 'price', precision: 24, default: () => '(0)::real' })
  price!: number;

  @Column('integer', { name: 'number_of_months', nullable: true })
  numberOfMonths!: number | null;

  @Column('integer', { name: 'number_of_codes', nullable: true })
  numberOfCodes!: number | null;

  @Column('text', { name: 'type', nullable: true })
  type!: string | null;

  @Column('text', { name: 'transaction_method' })
  transactionMethod!: string;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('text', { name: 'store_code', nullable: true })
  storeCode!: string | null;

  @Column('real', { name: 'discounted_price', nullable: true, precision: 24 })
  discountedPrice!: number | null;

  @OneToMany(() => Transactions, (transactions) => transactions.product)
  transactions!: Transactions[];
}

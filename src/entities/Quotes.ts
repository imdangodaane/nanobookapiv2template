import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('quotes_pkey', ['id'], { unique: true })
@Entity('quotes', { schema: 'public' })
export class Quotes {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', {
    name: 'quote_content',
    nullable: true,
    length: 1000,
  })
  quoteContent!: string | null;

  @Column('character varying', { name: 'author', nullable: true, length: 70 })
  author!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;
}

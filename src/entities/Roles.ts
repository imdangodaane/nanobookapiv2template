import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Users } from './Users';

@Index('roles_code_unique', ['code'], { unique: true })
@Index('roles_pkey', ['id'], { unique: true })
@Entity('roles', { schema: 'public' })
export class Roles {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'code', unique: true, length: 10 })
  code!: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  description!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled: boolean = false;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @OneToMany(() => Users, (users) => users.role)
  users!: Users[];
}

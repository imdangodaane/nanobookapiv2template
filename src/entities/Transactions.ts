import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Coupons } from './Coupons';
import { ProductPackages } from './ProductPackages';
import { Users } from './Users';

@Index('transactions_pkey', ['id'], { unique: true })
@Entity('transactions', { schema: 'public' })
export class Transactions {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('varchar', { name: 'status_track', array: true })
  statusTrack!: string[];

  @Column('character varying', { name: 'current_status', length: 255 })
  currentStatus!: string;

  @Column('text', { name: 'transaction_method' })
  transactionMethod!: string;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('text', { name: 'google_play_purchase_token', nullable: true })
  googlePlayPurchaseToken!: string | null;

  @Column('character varying', {
    name: 'coupon_used',
    nullable: true,
    length: 6,
  })
  couponUsed!: string | null;

  @Column('character varying', {
    name: 'failed_reason',
    nullable: true,
    length: 255,
  })
  failedReason!: string | null;

  @OneToMany(() => Coupons, (coupons) => coupons.transaction)
  coupons!: Coupons[];

  @ManyToOne(
    () => ProductPackages,
    (productPackages) => productPackages.transactions,
  )
  @JoinColumn([{ name: 'product_id', referencedColumnName: 'id' }])
  product!: ProductPackages;

  @ManyToOne(() => Users, (users) => users.transactions)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

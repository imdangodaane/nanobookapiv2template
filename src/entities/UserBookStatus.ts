import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { Users } from './Users';

@Index('user_book_status_pkey', ['id'], { unique: true })
@Entity('user_book_status', { schema: 'public' })
export class UserBookStatus {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('text', { name: 'status', nullable: true })
  status!: string | null;

  @Column('integer', { name: 'current_chapter', nullable: true })
  currentChapter!: number | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled: boolean = false;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @ManyToOne(() => Books, (books) => books.userBookStatuses)
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book: Books = new Books;

  @ManyToOne(() => Users, (users) => users.userBookStatuses)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: Users = new Users;
}

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Users } from './Users';

@Index('user_focus_results_pkey', ['id'], { unique: true })
@Entity('user_focus_results', { schema: 'public' })
export class UserFocusResults {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('integer', { name: 'focus_duration', nullable: true })
  focusDuration!: number | null;

  @Column('integer', { name: 'focus_count', nullable: true })
  focusCount!: number | null;

  @Column('integer', { name: 'number_of_touches', nullable: true })
  numberOfTouches!: number | null;

  @Column('real', { name: 'focus_percentage', nullable: true, precision: 24 })
  focusPercentage!: number | null;

  @Column('date', { name: 'result_date', nullable: true })
  resultDate!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @ManyToOne(() => Users, (users) => users.userFocusResults)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

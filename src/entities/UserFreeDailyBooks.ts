import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Books } from './Books';
import { Users } from './Users';

@Index('user_free_daily_books_pkey', ['id'], { unique: true })
@Entity('user_free_daily_books', { schema: 'public' })
export class UserFreeDailyBooks {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('date', { name: 'date_choosen', nullable: true })
  dateChoosen!: string | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled: boolean = false;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @ManyToOne(() => Books, (books) => books.userFreeDailyBooks)
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book: Books = new Books;

  @ManyToOne(() => Users, (users) => users.userFreeDailyBooks)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: Users = new Users;
}

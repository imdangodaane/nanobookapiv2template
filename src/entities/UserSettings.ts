import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Users } from './Users';

@Index('user_settings_pkey', ['id'], { unique: true })
@Entity('user_settings', { schema: 'public' })
export class UserSettings {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('text', { name: 'language', nullable: true })
  language!: string | null;

  @Column('boolean', { name: 'timer_enabled', nullable: true })
  timerEnabled!: boolean | null;

  @Column('integer', { name: 'timer_duration', nullable: true })
  timerDuration!: number | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('integer', { name: 'book_reader_font_size', nullable: true })
  bookReaderFontSize!: number | null;

  @Column('character varying', {
    name: 'book_reader_background_color',
    nullable: true,
    length: 20,
  })
  bookReaderBackgroundColor!: string | null;

  @Column('real', {
    name: 'book_reader_brightness',
    nullable: true,
    precision: 24,
  })
  bookReaderBrightness!: number | null;

  @ManyToOne(() => Users, users => users.userSettings)
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user!: Users;
}

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BookHighlights } from './BookHighlights';
import { Coupons } from './Coupons';
import { FavoriteBooks } from './FavoriteBooks';
import { Libraries } from './Libraries';
import { LoginSessions } from './LoginSessions';
import { Transactions } from './Transactions';
import { UserBookStatus } from './UserBookStatus';
import { UserFocusResults } from './UserFocusResults';
import { UserFreeDailyBooks } from './UserFreeDailyBooks';
import { UserSettings } from './UserSettings';
import { Memberships } from './Memberships';
import { Roles } from './Roles';

@Index('users_pkey', ['id'], { unique: true })
@Index('users_login_id_unique', ['loginId'], { unique: true })
@Entity('users', { schema: 'public' })
export class Users {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'login_id', unique: true, length: 255 })
  loginId!: string;

  @Column('character varying', {
    name: 'password',
    nullable: true,
    length: 255,
  })
  password!: string | null;

  @Column('character varying', { name: 'email', length: 200 })
  email!: string;

  @Column('character varying', { name: 'avatar', nullable: true, length: 255 })
  avatar!: string | null;

  @Column('text', { name: 'login_method', default: () => "'local'" })
  loginMethod!: string;

  @Column('timestamp with time zone', { name: 'expired_date', nullable: true })
  expiredDate!: Date | null;

  @Column('timestamp with time zone', {
    name: 'next_charge_at',
    nullable: true,
  })
  nextChargeAt!: Date | null;

  @Column('boolean', {
    name: 'is_trial_eligible',
    nullable: true,
    default: () => 'true',
  })
  isTrialEligible!: boolean | null;

  @Column('boolean', {
    name: 'is_trial',
    nullable: true,
    default: () => 'false',
  })
  isTrial!: boolean | null;

  @Column('timestamp with time zone', { name: 'trial_end_on', nullable: true })
  trialEndOn!: Date | null;

  @Column('character varying', {
    name: 'reset_password_token',
    nullable: true,
    length: 255,
  })
  resetPasswordToken!: string | null;

  @Column('timestamp with time zone', {
    name: 'reset_password_token_expire',
    nullable: true,
  })
  resetPasswordTokenExpire!: Date | null;

  @Column('boolean', { name: 'is_enabled', default: () => 'true' })
  isEnabled!: boolean;

  @Column('timestamp with time zone', {
    name: 'created_at',
    default: () => 'now()',
  })
  createdAt!: Date;

  @Column('timestamp with time zone', {
    name: 'updated_at',
    default: () => 'now()',
  })
  updatedAt!: Date;

  @Column('character varying', {
    name: 'first_name',
    nullable: true,
    length: 70,
  })
  firstName!: string | null;

  @Column('character varying', {
    name: 'last_name',
    nullable: true,
    length: 70,
  })
  lastName!: string | null;

  @Column('timestamp with time zone', { name: 'start_date', nullable: true })
  startDate!: Date | null;

  @Column('integer', { name: 'free_book_for_the_day', nullable: true })
  freeBookForTheDay!: number | null;

  @OneToMany(() => BookHighlights, (bookHighlights) => bookHighlights.user)
  bookHighlights!: BookHighlights[];

  @OneToMany(() => Coupons, (coupons) => coupons.usedBy)
  coupons!: Coupons[];

  @OneToMany(() => FavoriteBooks, (favoriteBooks) => favoriteBooks.user)
  favoriteBooks!: FavoriteBooks[];

  @OneToMany(() => Libraries, (libraries) => libraries.user)
  libraries!: Libraries[];

  @OneToMany(() => LoginSessions, (loginSessions) => loginSessions.user)
  loginSessions!: LoginSessions[];

  @OneToMany(() => Transactions, (transactions) => transactions.user)
  transactions!: Transactions[];

  @OneToMany(() => UserBookStatus, (userBookStatus) => userBookStatus.user)
  userBookStatuses!: UserBookStatus[];

  @OneToMany(
    () => UserFocusResults,
    (userFocusResults) => userFocusResults.user,
  )
  userFocusResults!: UserFocusResults[];

  @OneToMany(
    () => UserFreeDailyBooks,
    (userFreeDailyBooks) => userFreeDailyBooks.user,
  )
  userFreeDailyBooks!: UserFreeDailyBooks[];

  @OneToMany(() => UserSettings, (userSettings) => userSettings.user)
  userSettings!: UserSettings[];

  @ManyToOne(() => Memberships, (memberships) => memberships.users)
  @JoinColumn([{ name: 'membership_id', referencedColumnName: 'id' }])
  membership!: Memberships;

  @ManyToOne(() => Roles, (roles) => roles.users)
  @JoinColumn([{ name: 'role_id', referencedColumnName: 'id' }])
  role!: Roles;
}

import { App } from './app';

// Create new app instance
const app = new App();

// Running server
app.start();
